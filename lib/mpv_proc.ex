defmodule MPVMC.Process do
  use GenServer
  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl true
  def init(args) do
    :erlang.send(self(), {:connect, args})
    s = Keyword.new()
    ets = :ets.new(:mpv_incoming, [:private])
    {:ok, Keyword.put(s, :ets, ets)}
  end

  @impl true
  def handle_info({:connect, ipc} = cmd, state) do
    case :gen_tcp.connect({:local, ipc},0, [:binary, active: true, packet: :line, reuseaddr: true, packet_size: 10000, buffer: 100000]) do
      {:error, err} ->
        Logger.info("Connection failed to mpv socket #{ipc}, trying again")
        :erlang.send_after(1000, self(), cmd)
        {:noreply, state}
      {:ok, fd} ->
        Logger.info("Succesfully connected to mpv")
        {:noreply, Keyword.put(state, :ipc, fd)}
    end

  end


  @impl true
  def handle_info({:tcp, port, data}, state) do
#    Logger.debug("Received #{data}")
    d = Jason.decode!(data)
    case d do
      %{"event" => _ev} -> handle_event(d, state[:ets])
      %{"request_id" => id} -> handle_request(d, state[:ets])
    end
    {:noreply, state}

  end
  def handle_info({:tcp_closed, p}, state) do
    {:stop, state}
  end
  defp data_resp_to_tuple(d) do
    case d do
      %{"error" => "success", "data" => data} -> {:ok, data}
      %{"error" => "success"} -> :ok
      %{"error" => err} -> {:error, err}
    end
  end


  defp handle_event(%{"id" => id, "name" => name, "data" => data, "event" => ev}, ets_table) do
    [{^id, from}]  = :ets.lookup(ets_table, id)
    {pid, _tag} = from
    unless Process.alive?(pid) do
        :ets.delete_object(ets_table, {id,from})
    else
      Process.send(pid, {:mpv_property, ev, name, data}, [:nosuspend])
    end
  end

  defp handle_event(other, _ets_table) do
    IO.inspect other
  end

  defp handle_request(d, ets_table ) do
    id = d["request_id"]
    [{^id, from}]  = :ets.lookup(ets_table, id)
    GenServer.reply(from,  data_resp_to_tuple(d))
    :ets.delete_object(ets_table, {id,from})
  end

  defp send_mpv_cmd(cmd, p) do
    id = System.unique_integer([:positive])
 #   Logger.debug("Sending #{j}")
    j = Jason.encode_to_iodata!(%{command: cmd, request_id: id, async: true})
    :ok = :gen_tcp.send(p, j ++ ["\n"])
    id
  end

  @impl true
  def handle_cast({:command, command}, state)  do
    send_mpv_cmd(command, Keyword.get(state, :ipc))
    {:noreply, state}
  end

  @impl true
  def handle_call({:observe, prop}, from, state)  do
    p_id = System.unique_integer([:positive])
    id = send_mpv_cmd(["observe_property", p_id, prop], Keyword.get(state, :ipc))
    :ets.insert(state[:ets], [{id, from}, {p_id, from}])
    {:noreply, state}
  end

  @impl true
  def handle_call({:command, command}, from, state)  do
    id = send_mpv_cmd(command, Keyword.get(state, :ipc))
    :ets.insert(state[:ets], {id, from})
    {:noreply,  state}
  end

  def command(cmd)
    when is_list(cmd) do
    GenServer.call(__MODULE__, {:command, cmd})
  end

  def command(command, args \\ []) do
    command([command|args])
  end

  def observe_propery(prop) do
    GenServer.call(__MODULE__, {:observe, prop})
  end

  def get_property(prop) do
    GenServer.call(__MODULE__, {:command, ["get_property", prop]})
  end

  def get_property!(prop) do
    {:ok, d} = get_property(prop)
    d
  end


  def set_property(prop, value) do
    GenServer.call(__MODULE__, {:command, ["set_property", prop, value]})
  end

  def command_async(command, args \\ []) do
    GenServer.cast(__MODULE__, {:command, [command| args]})
  end

end

defmodule MPVMC.Application do
  use Application

  @impl true
  def start(_type, _args) do
    MPVMC.Supervisor.start_link(name: MPVMC.Supervisor)
  end

end



defmodule MPVMC.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    home = System.user_home!()
    ipc = Path.join(home, ".mpvhc.sock")

    children = [
      {MuonTrap.Daemon, ["mpv",
                         ["--profile=pseudo-gui", "--keep-open", "--input-ipc-server=#{ipc}", "--log-file=mpv.log", "--no-config", "--idle"]]},
      {MPVMC.Process, ipc},
      {Plug.Cowboy, scheme: :http, plug: MPVMC.HTTPServer, options: [port: 4001]}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

end

defmodule MPVMC.HTTPServer do
  use Plug.Router
  use Plug.ErrorHandler
  import Phoenix.HTML

  @valid_extensions MapSet.new(Enum.map(["264","265","3g2","3ga","3ga2","3gp","3gp2","3gpp","3iv","a52","aac","adt","adts","ahn","aif","aifc","aiff","amr","ape","asf","au","avc","avi","awb","ay",
    "bmp","cue","divx","dts","dtshd","dts-hd","dv","dvr","dvr-ms","eac3","evo","evob","f4a","flac","flc","fli","flic","flv","gbs","gif","gxf","gym",
    "h264","h265","hdmov","hdv","hes","hevc","jpeg","jpg","kss","lpcm","m1a","m1v","m2a","m2t","m2ts","m2v","m3u","m3u8","m4a","m4v","mk3d","mka","mkv",
    "mlp","mod","mov","mp1","mp2","mp2v","mp3","mp4","mp4v","mp4v","mpa","mpe","mpeg","mpeg2","mpeg4","mpg","mpg4","mpv","mpv2","mts","mtv","mxf","nsf",
    "nsfe","nsv","nut","oga","ogg","ogm","ogv","ogx","opus","pcm","pls","png","qt","ra","ram","rm","rmvb","sap","snd","spc","spx","svg","thd","thd+ac3",
    "tif","tiff","tod","trp","truehd","true-hd","ts","tsa","tsv","tta","tts","vfw","vgm","vgz","vob","vro","wav","weba","webm","webp","wm","wma","wmv","wtv",
    "wv","x264","x265","xvid","y4m","yuv"], fn x -> "." <> x end))
  @root "/mnt/pub/"

  plug Plug.Logger
  plug Plug.Parsers,
     parsers: [:urlencoded, :json],
     json_decoder: Jason
  plug Plug.Static, from: {:mpvmc, "priv/"}, at: "/"
  plug :match
  plug :dispatch
  # @dispatch :cowboy_router.compile([
  #   {'_',[
  #       {"/api/property/:property", get_property, %{}},
  #       {"/api/property/:property/:value", set_property, %{}},
  #       {"/api/command/[...]", run_command, %{}},
  #       {"/api/loadfile", loadfile, %{}},
  #     ]}
  # ])

  defp json_resp(conn, code, res, other \\ []) do
    j =
      Enum.into(other, %{result: Atom.to_string(res)})
      |> Jason.encode!()
    conn
    |> Plug.Conn.put_resp_content_type("application/json")
    |> send_resp( code, j)
  end

  defp genFilepickerElem(name, type) do
    {icon,data} =
      case type do
        :dir -> {"folder-open",~E"""
        ts-data=newdir=<%= name %> ts-req=/api/ts/filepickertable ts-target="#filepicker"  ts-trigger=click
        """}
        :file -> {"file", ~E"""
        ts-data="file=<%= name %>" ts-req=/api/ts/loadfile ts-target="parent table" ts-trigger=click
        """}
      end

    ~E"""

    <table class="playlist">
    <tr class="playlist"  <%= data %> ><td class="playlist gray">
        <i class="fas fa-<%= icon %>"></i>
        </td>  <td class="playlist gray"><%= name %>
        </td></tr>
    </table>
    """
  end

  defp genFilepickerList(dir, newdir) when is_nil(dir) do
    wd = MPVMC.Process.get_property!("working-directory")
    path = MPVMC.Process.get_property!("path")

    Path.expand(path, wd)
    |> Path.dirname
    |> genFilepickerList("")
  end

  defp genFilepickerList(dir, newdir) when is_nil(newdir) do
    genFilepickerList(dir, "")
  end

  defp genFilepickerList(dir, newdir) do
    dir =
      Path.join(dir, newdir)
      |> Path.expand()
    elems =
      case File.ls(dir) do
        {:ok, files} ->
          Enum.sort(files)
          |> Enum.flat_map(fn x -> p = Path.join(dir, x)
            cond do
                String.starts_with?(x, ".") -> [] # hidden files
              File.dir?(p) ->  [genFilepickerElem(x, :dir)]
              File.regular?(p) and Path.extname(p) in @valid_extensions -> [genFilepickerElem(x, :file)]
              true -> []
            end
          end)
        {:error, err} ->
          []
      end
    ~E"""
     <div id="filepicker"  ts-data="dir=<%= dir %>" ts-req="/api/ts/filepickertable">
     <table class="playlist-control"><tr><td>
     <div ts-req="/api/ts/filepickertable" ts-target="#filepicker" ts-trigger="click" ts-data="newdir=.." class="button violet content icon-content playlist-controls">
     <i class="fas fa-level-up-alt"></i>
     </div>
     </td><td>
     <div onClick="toggleOverlay('filepicker-overlay')" class="button violet content icon-content playlist-controls">
     <i class="fas fa-times-circle"></i>
     </div></td></tr>
     </table>
     <div >
     <%= elems %>
     </div>
     </div>
    """
    |> Phoenix.HTML.Safe.to_iodata()

  end

  defp loadfile(conn) do
    dir = conn.query_params["dir"]
    file = conn.query_params["file"]
    filename = Path.join(dir,file)
    MPVMC.Process.command(["osd-auto", "loadfile", filename, "append-play"])
    data = Phoenix.HTML.Safe.to_iodata(genFilepickerElem(file, :file))
    conn =  send_resp(conn, 200, data)
  end

  get "/api/ts/loadfile" do
    conn
    |> Plug.Conn.fetch_query_params
    |> Plug.Conn.put_resp_content_type("text/html")
    |> loadfile

  end

  get "/api/ts/filepickertable" do
    conn
    |> Plug.Conn.fetch_query_params
    |> Plug.Conn.put_resp_content_type("text/html")
    |> send_resp(200, genFilepickerList(conn.query_params["dir"], conn.query_params["newdir"]))

  end


  get "/api/property/:property" do
    case MPVMC.Process.get_property(property) do
      {:ok, data} ->
        json_resp(conn, 200, :success, value: data)
      {:error, err} ->
        json_resp(conn, 500, :error, error: err)
    end
  end

  post "/api/property/:property/:value" do
    case MPVMC.Process.set_property(property, value) do
      :ok ->
        json_resp(conn, 200, :success)
      {:error, err} ->
        json_resp(conn, 500, :error, error: err)
    end
  end

  defp run_command(conn, cmd) do
    case MPVMC.Process.command(cmd) do
      :ok ->
        json_resp(conn, 200, :success)
      {:ok, data} ->
        json_resp(conn, 200, :success, value: data)
      {:error, err} ->
        json_resp(conn, 500, :error, error: err)
    end
  end

  get "/api/command/:cmd/*args" do
    run_command(conn, [cmd|args])
  end

  post "/api/loadfile" do
    case conn.body_params do
      %{"file" => f} -> run_command(conn, ["loadfile", Path.join(@root,f), "append-play"])
      _ -> {:error, "Could not find command"}
    end
  end

  post "/api/command" do
    case conn.body_params do
      %{"command" => cmd} -> run_command(conn, cmd)
      _ -> {:error, "Could not find command"}
    end
  end

  defp prop_iter(x, acc) do
    case MPVMC.Process.get_property(x) do
      {:ok, data} -> Map.put(acc, x, data)
      {:error, _err} -> Map.put(acc, x, "")
    end
  end

  get "/api/status" do
    j = Enum.reduce(
      ["speed", "pause", "fullscreen", "filename",
       "playlist", "metadata", "sub-delay", "volume-max",
       "volume", "track-list", "duration", "playtime-remaining",
       "audio-delay", "chapter", "chapters", "chapter-list", "time-pos",
       "loop-file", "loop-playlist"      ],
      %{}, fn x,acc -> prop_iter(x,acc) end)
    |> Jason.encode!()
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, j)
   end

  get "/api/readdir/*args" do
    path =  Path.join([@root|args])
    contents = File.ls(path)
    case contents do
      {:ok, files} ->
        m =
          Enum.sort(files)
          |> Enum.flat_map(fn x -> p = Path.join(path, x)
          cond do
            File.dir?(p) ->  [%{name: x, type: :dir}]
            File.regular?(p) and Path.extname(p) in @valid_extensions -> [%{name: x, type: :file}]
            true -> []
          end
        end)
          json_resp(conn, 200, :success, value: m)

        {:error, err} ->
        json_resp(conn, 500, :error, error: err)
    end
  end

  match "/" do
    conn
    |> Plug.Conn.put_resp_header("location", "index.html")
    |> send_resp(301, "/index.html")
  end

  match _ do
#    put_resp_content_type("text/plain")
    send_resp(conn, 404, "oops")
  end

  defp handle_errors(conn, %{kind: kind, reason: reason, stack: stack}) do
    IO.inspect(kind, label: :kind)
    IO.inspect(reason, label: :reason)
    IO.inspect(stack, label: :stack)
    send_resp(conn, conn.status, "Something went wrong")
  end
end

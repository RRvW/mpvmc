{ pkgs ? import <nixpkgs> { } }:

let pkgsUnstable = import <nixpkgs-unstable> { };
in pkgs.mkShell {
  buildInputs = [
    pkgs.socat
    pkgs.jq
    pkgsUnstable.elixir_1_11
    pkgsUnstable.nodePackages.typescript-language-server
    pkgsUnstable.elixir_ls
    # keep this line if you use bash
    pkgs.bashInteractive
  ];
  LANG = "en_US.UTF-8";
  LC_ALL = "en_US.UTF-8";

}

defmodule MPVMC.MixProject do
  use Mix.Project

  def project do
    [
      app: :mpvmc,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {MPVMC.Application, {}}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
	    {:muontrap, "~> 0.6.0"},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:jason, "~> 1.2"},
      {:plug, "~> 1.11"},
      {:plug_cowboy, "~> 2.0"},
      {:phoenix_html, "~> 2.14"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
